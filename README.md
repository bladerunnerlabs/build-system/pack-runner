# pack-runner

## Format of packaging configuration files

The YAML packaging configuration files have a recursively defined format.

First, configuration YAML files may include other configuration YAMLs.

Second, these files are organized as nested lists of packaging directives for individual directories.

Packaging of a directory is controlled by a `DIR` directive.

Each yaml file (root or included) describes an implicit `DIR` entry describing a directory. By default its path is the current directory: `.`

Each directory is covered by a YAML array element having three keys:
* `DEST: <relative or abs path>` - the destination directory after the tarball is unpacked
* `PACK`: and array of packaging elements

each PACK list item must be one of the following:
* `INCLUDE <yaml-name>`
* `FILES <files>`
  * `<files>` :
    * list of file names and/or patterns, e.g.:
      * `[ main.cpp api.cpp api.h ]`
      * `[ config_*.py ]`
    * `ALL` - all files on the directory
    * `NONE` - no files
  * attributes:
    * `DEST`
* `DIR <relative or abs path>`; attributes:
  * `DEST`
  * `PACK` - recursive
* `TREE <relative or abs path>`; attributes:
  * `DEST`
  * `FILES`

## tarpack.py command line interface
```
usage: tarpack.py [-h] [-q] <command> [cmd-options]

tarpack handling helper

  -h, --help            show this help message and exit
  -q, --quiet           quiet output option [default: false]

> commands:
{pack,purge,unpack}
    pack               pack commands
    purge              purge commands
    unpack             unpack commands

> pack options:
[-d DIR] -y YAML [-k] [-o OUT] [-t TAR_DIR] [-T TAG]

optional arguments:
  -d DIR, --dir DIR     YAML files work directory [default: .] (valid for all
                        consequent YAML files)
  -y YAML, --yaml YAML  <yaml_file> : tarpack YAML file name (multiple entries
                        allowed; relative to the latest work dir)
  -k, --keep            keep tar (symlinks only) source directory [default:
                        remove]
  -o OUT, --out OUT     <out_tgz> : tar.gz output file path [default:
                        ./pack.tgz]
  -t TAR_DIR, --tar-dir TAR_DIR
                        <tar_dir> : tar (symlinks only) source directory path
                        [default: .]'
  -T TAG, --tag TAG     <tag_name> : tag to be matched against TAGS list,
                        multiple values accepted

> unpack options:
-t TAR_PATH -o OUT_DIR

optional arguments:
  -t TAR_PATH, --tar-path TAR_PATH
                        <tar_path> : full tarfile path
  -o OUT_DIR, --out-dir OUT_DIR
                        <out_dir> : directory for unpacking

> purge options:
-d DIR -y YAML

optional arguments:
  -d DIR, --dir DIR     YAML files work directory [default: .] (valid for all
                        consequent YAML files)
  -y YAML, --yaml YAML  <yaml_file> : tarpack YAML file name (multiple entries
                        allowed; relative to the latest work dir)

```
