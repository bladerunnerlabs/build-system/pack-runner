#!/usr/bin/env python3
#
# Copyright (c) 2019-2021 - BladeRunner Labs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the "Software")
# to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import sys
import getopt
import glob
import yaml
import tarfile
import re
import argparse
import subprocess
from shutil import rmtree, which
from os import makedirs, symlink, remove, getcwd, chdir, walk
from os.path import basename, dirname, isfile, exists, join, expandvars, normpath

tarpack_dir_parent = '.'
tarpack_dir_name = '.tarpack'
archive_fname = 'pack.tgz'
del_tarpack_dir = True
files = []
tags_list = []
command = 'pack'
quiet_output = False


def fatal_error(cfg_file, msg1, msg2=None, msg3=None, msg4=None, msg5=None):
    print()
    if cfg_file:
        print("ERROR: " + cfg_file)
    if msg1:
        print(msg1)
    if msg2:
        print(msg2)
    if msg3:
        print(msg3)
    if msg4:
        print(msg4)
    if msg5:
        print(msg5)
    sys.exit(1)


def concat_dest(base_path, dest, ignore_root=False):
    if not ignore_root:
        # if dest starts from root: '/', path is ignored
        dest_path = dest
    else:
        # concat dest even if it starts from root
        dest_path = dest.lstrip('/')
    return normpath(join(base_path, dest_path))


def tar_print_name(tarinfo):
    if not tarinfo.isdir():
        if not quiet_output:
            print(tarinfo.name + " : " + str(tarinfo.size))
    return tarinfo


def lists_intersect(list1, list2):
    return [value for value in list1 if value in list2]


def select_tags(item_tags):
    if not isinstance(item_tags, list):
        item_tags = [item_tags]
    return lists_intersect(item_tags, tags_list)


def create_symlink(src_path, symlink_path, cfg_file, force=False):
    try:
        if not exists(src_path):
            fatal_error(cfg_file, "\nsymlink src file not found:", src_path)
        symlink(src_path, symlink_path)
    except FileExistsError as err:
        if force:
            if not quiet_output:
                print("\nsymlink (dst) already exists:\n" + str(err))
                print("\nconfig_file: " + cfg_file)
                print("REPLACE=True, Force create...\n")
            remove(symlink_path)
            create_symlink(src_path, symlink_path, cfg_file, force)
        else:
            fatal_error(cfg_file, str(err),
                        "\nsymlink (dst) already exists, but REPLACE=False:",
                        tarpack_dst_path)
    except PermissionError:
        fatal_error(cfg_file, str(err), "\npermission denied:",
                    tarpack_dst_path)
    except FileNotFoundError:
        fatal_error(cfg_file, str(err), "\nsymlink src file not found:",
                    src_path)


def read_yaml(yaml_path):
    if not exists(yaml_path):
        fatal_error(yaml_path, "YAML file not found")

    pattern = re.compile(r'.*\$\{([^}^{]+)\}.*')

    def env_constructor(loader, node):
        return expandvars(node.value)

    class EnvVarLoader(yaml.SafeLoader):
        pass

    EnvVarLoader.add_implicit_resolver('!env', pattern, None)
    EnvVarLoader.add_constructor('!env', env_constructor)

    with open(yaml_path, 'r') as yaml_stream:
        try:
            return yaml.load(yaml_stream, Loader=EnvVarLoader)
        except yaml.YAMLError as yaml_err:
            fatal_error(yaml_path, str(yaml_err), "invalid YAML syntax")


def handle_pack_list(cfg_file, pack_list, path, dest=None):
    for item in pack_list:
        handle_pack_item(cfg_file, item, path, dest)


def handle_pack_item(cfg_file, item, path, dest):
    if 'INCLUDE' in item:
        handle_include_item(cfg_file, item, path, dest)
    elif 'DIR' in item:
        handle_dir_item(cfg_file, item, path, dest)
    elif 'TREE' in item:
        handle_tree_item(cfg_file, item, path, dest)
    elif 'FILES' in item:
        handle_files_item(cfg_file, item, path, dest)
    else:
        fatal_error(cfg_file, "Unsupported PACK list item type", item)


def verify_implied_dir_item(cfg_file, yaml):
    if isinstance(yaml, list):
        fatal_error(cfg_file,
                    "list detected; a single DIR item content is implied",
                    "list items can be added to the PACK list")

    if 'DIR' in yaml:
        fatal_error(cfg_file, "explicit DIR item; implied DIR item content",
                    "DIR items can be added to the PACK list")

    if 'PACK' not in yaml:
        fatal_error(cfg_file, "PACK list missing")

    if not isinstance(yaml['PACK'], list):
        fatal_error(cfg_file, ": PACK item must be a list")


def handle_include_item(cfg_file, item, path, dest):
    include_file = concat_dest(path, item['INCLUDE'])
    include_dir = dirname(include_file)

    if 'TAGS' in item:
        selected_tags = select_tags(item['TAGS'])
        if selected_tags:
            if not quiet_output:
                print(cfg_file + " INCLUDE: " + include_file +
                      ", selected tags " + str(selected_tags))
        else:
            if not quiet_output:
                print(cfg_file + " INCLUDE: " + include_file +
                      ", skipped tags " + str(item['TAGS']))
            return

    include_yaml = read_yaml(include_file)
    verify_implied_dir_item(cfg_file, include_yaml)

    if not quiet_output:
        print(cfg_file + ": INCLUDE " + include_file + " DEST " + dest)
    handle_dir_item(include_file, include_yaml, include_dir, dest)


def handle_files_item(cfg_file, item, path, dest):
    if 'DEST' in item:
        item['DEST'] = concat_dest(dest, item['DEST'])
    else:
        item['DEST'] = dest

    if not item['DEST']:
        fatal_error(cfg_file, "DEST unresolved", item)

    if 'TAGS' in item:
        selected_tags = select_tags(item['TAGS'])
        if selected_tags:
            if not quiet_output:
                print(cfg_file + " FILES: selected tags " + str(selected_tags))
        else:
            if not quiet_output:
                print(cfg_file + " FILES: skipped tags " + str(item['TAGS']))
            return

    handle_files_list(cfg_file, item['FILES'], path, item['DEST'])


def handle_dir_item(cfg_file, item, path, dest):
    if isinstance(item, list):
        fatal_error(cfg_file, "DIR item is a list",
                    "list items can be added to the PACK list")

    if 'PACK' not in item:
        fatal_error(cfg_file, ": PACK list missing")

    if not isinstance(item['PACK'], list):
        fatal_error(cfg_file, ": PACK item must be a list")

    if 'TAGS' in item:
        selected_tags = select_tags(item['TAGS'])
        if selected_tags:
            if not quiet_output:
                print(cfg_file + " DIR: selected tags " + str(selected_tags))
        else:
            if not quiet_output:
                print(cfg_file + " DIR: skipped tags " + str(item['TAGS']))
            return

    if dest:
        if 'DEST' in item:
            item['DEST'] = concat_dest(dest, item['DEST'])
        else:
            item['DEST'] = dest
    elif 'DEST' not in item:
        fatal_error(cfg_file, ": DEST missing")

    if 'DIR' in item:
        if path:
            item['DIR'] = concat_dest(path, item['DIR'])
    else:
        item['DIR'] = path

    handle_pack_list(cfg_file, item['PACK'], item['DIR'], item['DEST'])


def handle_tree_item(cfg_file, item, path, dest):
    if 'TAGS' in item:
        selected_tags = select_tags(item['TAGS'])
        if selected_tags:
            if not quiet_output:
                print(cfg_file + " TREE " + item['TREE'] + ": selected tags " +
                      str(selected_tags))
        else:
            if not quiet_output:
                print(cfg_file + " TREE " + item['TREE'] + ": skipped tags " +
                      str(item['TAGS']))
            return

    if 'DEST' in item:
        item['DEST'] = concat_dest(dest, item['DEST'])
    else:
        item['DEST'] = dest

    item['DIR'] = concat_dest(path, item['TREE'])

    if 'FILES' not in item:
        item['FILES'] = '*'

    if 'FLAT' in item:
        if isinstance(item['FLAT'], bool):
            dst_flat = item['FLAT']
        else:
            fatal_error(
                cfg_file, "TREE " + item['TREE'] +
                " contains non-boolean FLAT: " + item['FLAT'])
    else:
        dst_flat = False

    if isinstance(item['FILES'], list):
        for filter in item['FILES']:
            handle_tree_filter(cfg_file, item['DIR'], item['DEST'], filter,
                               dst_flat)
    else:
        handle_tree_filter(cfg_file, item['DIR'], item['DEST'], item['FILES'],
                           dst_flat)


def handle_files_list(cfg_file, files_list, path, dest):
    if isinstance(files_list, list):
        for file_name in files_list:
            handle_file(cfg_file, file_name, path, dest)
    else:
        handle_file(cfg_file, files_list, path, dest)


def handle_file(cfg_file, file_name, path, dest):
    if file_name == 'ALL':
        file_name = '*'

    path_spec = concat_dest(path, file_name)
    if command == "pack":
        if '*' in file_name:
            expand_list = list_files(path_spec)
            handle_files_list(cfg_file, expand_list, path, dest)
        elif (file_name != 'NONE'):  # and (path_spec != cfg_file):
            if not quiet_output:
                print(cfg_file + ": SRC " + path + " DEST " + dest + " FILE " +
                      file_name)
            files.append({
                'src': path,
                'dest': dest,
                'name': file_name,
                'cfg': cfg_file
            })
    elif command == "purge":
        if not quiet_output:
            print("rm: %s" % (path_spec))
        os.remove(path_spec)


def unique_suffix(a, b):
    return b[([x[0] == x[1] for x in zip(a, b)] + [0]).index(0) + 1:]


def handle_tree_filter(cfg_file, root_path, dest, filter, dst_flat):
    if not quiet_output and command == "pack":
        print(cfg_file + ": TREE " + root_path + " DEST " + dest +
              (" FLAT" if dst_flat else " PRESERVE") + " FILES " + filter)

    if filter == 'ALL':
        filter = '*'

    if dirname(filter) != '':
        fatal_error(
            cfg_file, "TREE " + root_path + ", FILES filter '" + filter +
            "' contains a path",
            "TREE FILE filters may be only file names, full or regular expressions"
        )
    if command == "purge" and filter == '*':
        if not quiet_output:
            print("rm: %s" % (root_path))
        rmtree(root_path, ignore_errors = True)
    else:
        expand_list = list_files(join(root_path, filter))
        handle_files_list(cfg_file, expand_list, root_path, dest)

        for root, subdirs, filenames in walk(root_path):
            for d in subdirs:
                d_path = join(root, d)
                expand_list = list_files(join(d_path, filter))
                if dst_flat:
                    d_dest = dest
                else:
                    suffix = unique_suffix(root_path, d_path)
                    d_dest = join(dest, suffix)
                handle_files_list(cfg_file, expand_list, d_path, d_dest)


def list_files(path):
    try:
        return [basename(f) for f in glob.glob(path) if isfile(f)]
    except:
        return []


def handle_yaml(yaml_dir, yaml_name):
    yaml_path = yaml_dir + '/' + yaml_name
    base_yaml = read_yaml(yaml_path)
    verify_implied_dir_item(yaml_path, base_yaml)
    if not quiet_output:
        print("\n" + yaml_path + ": ROOT")
    handle_dir_item(yaml_path, base_yaml, yaml_dir, None)


def validate_yaml_files_list(work_dir, yaml_files_list):
    ylist = []
    for yaml_name in yaml_files_list:
        yf = work_dir + '/' + yaml_name
        if not exists(yf):
            fatal_error("YAML", "file does not exist: ", yf)

        ylist.append({'dir': work_dir, 'name': yaml_name})
    return ylist


def pack_purge_handler(args):

    dir_dict = args.dirs.get_dict()

    for key in dir_dict.keys():
        yaml_files = validate_yaml_files_list(key, dir_dict[key])
        for yf in yaml_files:
            handle_yaml(yf['dir'], yf['name'])

    if command == "pack":
        if args.tag:
            tags_list = args.tag

        del_tarpack_dir = not args.keep

        archive_fname = args.out
        if not quiet_output:
            print('output tar file path: ', archive_fname)

        tarpack_dir_parent = args.tar_dir

        if not quiet_output:
            print('tarpack symfile parent dir: ', tarpack_dir_parent)

        # re-create the packing dir
        tarpack_dir_path = tarpack_dir_parent + '/' + tarpack_dir_name
        rmtree(tarpack_dir_path, ignore_errors=True)
        makedirs(tarpack_dir_path, exist_ok=True)

        # create symlinks to the packed files
        if not quiet_output:
            print("\nCreating symlinks in: " + tarpack_dir_path + " ...")

        for file in files:
            file_name = file['name']
            src_dir = file['src']
            dst_dir = file['dest']

            # src path must be full, referenced from the tarpack symlinks dir
            src_path = concat_dest(cwd, src_dir + '/' + file_name)
            # dst path should be relative to the tarpack symlinks dir
            dst_path = dst_dir + '/' + file_name
            if not quiet_output:
                print(src_path + ' --> ' + dst_path)

            makedirs(tarpack_dir_path + '/' + dst_dir, exist_ok=True)
            tarpack_dst_path = concat_dest(tarpack_dir_path,
                                           dst_path,
                                           ignore_root=True)
            create_symlink(src_path, tarpack_dst_path, file['cfg'], True)

        # create archive
        if not quiet_output:
            print("\nCreating archive file: " + tarpack_dir_path + " --> " +
                  archive_fname + " ... ")

        if which("pigz"):
            if not quiet_output:
                print("using pigz to compress...")

            # compress using pigz
            chdir(tarpack_dir_path)
            tar_pigz_cmd = 'tar cf - * --dereference | pigz -6 -p 32'

            try:
                archive_fh = open(archive_fname, "wb")
                pigz_process = subprocess.run(tar_pigz_cmd,
                                              shell=True,
                                              stdout=archive_fh)
                archive_fh.close()
            except:
                print("ERROR. Can not create %s" % (archive_fname))
                sys.exit(1)

        else:
            if not quiet_output:
                print("pigz not found, using tar.gz to compress...")

            # compress using tag.gz
            try:
                archive = tarfile.open(archive_fname,
                                       "w:gz",
                                       dereference=True,
                                       compresslevel=2)
                chdir(tarpack_dir_path)
                archive.add('.', filter=tar_print_name)
                archive.close()
            except:
                print("ERROR. Can not create %s" % (archive_fname))
                sys.exit(1)

        if del_tarpack_dir:
            if not quiet_output:
                print('\nRemove symlink dir: ' + tarpack_dir_path)
            rmtree(tarpack_dir_path, ignore_errors=True)
        else:
            if not quiet_output:
                print('\nKeep symlink dir: ' + tarpack_dir_path)

        print("\nCreated archive file: %s" % (archive_fname))

        if not quiet_output:
            print("Size: %s\n" % (os.path.getsize(archive_fname)))

        if not quiet_output:
            print("Done\n")


def unpack_progress(members):
    if quiet_output:
        return

    for member in members:
        # this will be the current file being extracted
        print(member)


def unpack_handler(args):
    tar_file = args.tar_path
    extract_dir = args.out_dir

    if which("pigz"):
        tar_unpigz_cmd = [
            "tar", "-I", "pigz", "-xvf", tar_file, "-C", extract_dir
        ]

        if not quiet_output:
            print("using pigz to decompress...")
            rc = subprocess.run(tar_unpigz_cmd).returncode
        else:
            rc = subprocess.run(tar_unpigz_cmd,
                                stdout=subprocess.DEVNULL,
                                stderr=subprocess.DEVNULL).returncode
        if rc != 0:
            print("ERROR. failed to decompress %s" % ' '.join(tar_unpigz_cmd))
            sys.exit(1)
    else:
        try:
            with tarfile.open(tar_file, 'r') as archive:
                if not quiet_output:
                    print("pigz not found, using tar.gz to decompress...")
                    archive.extractall(extract_dir,
                                       members=unpack_progress(archive))
                else:
                    archive.extractall(extract_dir)
                archive.close()
        except:
            print("ERROR. Can not open %s" % (tar_file))
            sys.exit(1)

    print("%s extracted successfully to %s" % (tar_file, extract_dir))


class ActionDir(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        if not 'dirs' in namespace:
            self.dest = {}
            setattr(namespace, 'dirs', self)
        value = values[0]
        dirs_dict = self.dest
        if value not in dirs_dict:
           dirs_dict[value] = [] # start with empty yaml list
        self.cur_dir = value

    def get_dict(self):
        return self.dest

class ActionYaml(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        if not 'dirs' in namespace:
            raise ValueError("no --dir defined")
        dirs_obj = namespace.dirs
        dirs_dict = dirs_obj.dest
        cur_dir = dirs_obj.cur_dir
        value = values[0]
        dirs_dict[cur_dir].append(value)


if __name__ == "__main__":

    cwd = getcwd()
    work_dir = cwd

    # initialize arg parser
    parser = argparse.ArgumentParser(description='tarpack handling helper')
    subparsers = parser.add_subparsers(dest='command')
    parser_pack = subparsers.add_parser('pack', help="pack commands")
    parser_purge = subparsers.add_parser('purge', help="purge commands")
    parser_unpack = subparsers.add_parser('unpack', help="unpack commands")

    parser_purge.add_argument(
        '-d',
        '--dir',
        nargs=1, action=ActionDir,
        help=
        "YAML files work directory [default: .] (valid for all consequent YAML files)"
        )

    parser_purge.add_argument(
        '-y',
        '--yaml',
        nargs=1, action=ActionYaml,
        help=
        "<yaml_file> : tarpack YAML file name (multiple entries allowed; relative to the latest work dir)"
    )

    parser_pack.add_argument(
        '-d',
        '--dir',
        nargs=1, action=ActionDir,
        help=
        "YAML files work directory [default: .] (valid for all consequent YAML files)"
        )

    parser_pack.add_argument(
        '-y',
        '--yaml',
        nargs=1, action=ActionYaml,
        help=
        "<yaml_file> : tarpack YAML file name (multiple entries allowed; relative to the latest work dir)"
    )

    parser_pack.add_argument(
        '-k',
        '--keep',
        required=False,
        action='store_true',
        help="keep tar (symlinks only) source directory [default: remove]",
        default=False)

    parser_pack.add_argument(
        '-o',
        '--out',
        required=False,
        help="<out_tgz> : tar.gz output file path [default: ./" +
        archive_fname + "]",
        default=archive_fname)

    parser_pack.add_argument(
        '-t',
        '--tar-dir',
        required=False,
        help=
        "<tar_dir> : tar (symlinks only) source directory path [default: .]'",
        default=tarpack_dir_parent)

    parser_pack.add_argument(
        '-T',
        '--tag',
        required=False,
        nargs='+',
        type=str,
        help=
        "<tag_name> : tag to be matched against TAGS list, multiple values accepted"
    )

    parser.add_argument('-q',
                        '--quiet',
                        required=False,
                        action='store_true',
                        help="quiet output option [default: false]",
                        default=False)

    parser_unpack.add_argument('-t',
                               '--tar-path',
                               required=True,
                               help="<tar_path> : full tarfile path")

    parser_unpack.add_argument('-o',
                               '--out-dir',
                               required=True,
                               help="<out_dir> : directory for unpacking")

    (args, extra_args) = parser.parse_known_args()

    command = args.command
    quiet_output = args.quiet

    if command == "pack" or command == "purge":
        pack_purge_handler(args)
    elif command == "unpack":
        unpack_handler(args)
